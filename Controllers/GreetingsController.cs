﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Localization.SqlLocalizer.IntegrationTests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace LocalizationAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GreetingsController : ControllerBase
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IStringLocalizer<GreetingsController> _greetingsLocalizerizer;

        public GreetingsController(IStringLocalizer<SharedResource> localizer, IStringLocalizer<GreetingsController> greetingsLocalizerizer)
        {
            _localizer = localizer;
            _greetingsLocalizerizer = greetingsLocalizerizer;
        }

        [HttpGet]
        public string Get()
        {
            // _localizer["Name"]
            //var culture = Request.Headers["Accept-Language"];
            //CultureInfo uiCultureInfo = Thread.CurrentThread.CurrentUICulture;
            //CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            ////CultureInfo.CurrentCulture = new CultureInfo("zh-CN");
            return _greetingsLocalizerizer["Greetings"];
        }
    }
}
